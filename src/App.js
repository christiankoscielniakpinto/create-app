import './App.css';
import Navbar from './components/Navbar';
import Utenti from './components/Utenti';

function App() {
  const utenti = [
    {
        id: "e1",
        nome: "Christian",
        cognome: "Koscielniak Pinto",
    },
    {
        id: "e2",
        nome: "Gennaro",
        cognome: "Pianese",
    },
    {
        id: "e3",
        nome: "Federico",
        cognome: "Pintus",
    },
    {
        id: "e4",
        nome: "Francesco",
        cognome: "Pace",
    },
];
  return (
    <div className="App">
      <Navbar />
      <h1 className="text-center">Lista Utenti</h1>
      <Utenti items={utenti}/>
    </div>
  );
}

export default App;
