import "./Utenti.css";
import Card from "./Card";
import UtenteItem from "./UtenteItem";

function Utenti(props) {

    return (
        <Card className="utenti">
            <UtenteItem
                nome={props.items[0].nome}
                cognome={props.items[0].cognome}
            />
            <UtenteItem
                nome={props.items[1].nome}
                cognome={props.items[1].cognome}
            />
            <UtenteItem
                nome={props.items[2].nome}
                cognome={props.items[2].cognome}
            />
            <UtenteItem
                nome={props.items[3].nome}
                cognome={props.items[3].cognome}
            />
        </Card>
    );
}

export default Utenti;