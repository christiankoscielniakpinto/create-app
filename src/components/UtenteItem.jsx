
import Card from "./Card";
import "./UtenteItem.css";


function UtenteItem(props) {
    return (
        <Card className="utente-item">
            <div className="utente-item__description">
                <h2>{props.nome}</h2>
                <div className="utente-item__price">{props.cognome}</div>
            </div>
        </Card>
    );
}

export default UtenteItem;